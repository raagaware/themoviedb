//
//  Configuration.swift
//  TheMovieDB
//
//  Created by Suman Sanyal on 12/12/21.
//

import Foundation

struct Configuration {
    
    var imageBaseUrl: String
    var imageSecureBaseUrl: String
    var backdropSizes: [String]
    var logoSizes: [String]
    var posterSizes: [String]
    var profileSizes: [String]
    var stillSizes: [String]

    var changeKeys: [String]
}

extension Configuration {
    
    static func fromApi(response: ApiConfiguration) -> Configuration {
        Configuration(imageBaseUrl: response.images.baseUrl,
                      imageSecureBaseUrl: response.images.secureBaseUrl,
                      backdropSizes: response.images.backdropSizes,
                      logoSizes: response.images.logoSizes,
                      posterSizes: response.images.posterSizes,
                      profileSizes: response.images.profileSizes,
                      stillSizes: response.images.stillSizes,
                      changeKeys: response.changeKeys)
    }
}
