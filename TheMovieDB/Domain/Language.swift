//
//  Language.swift
//  TheMovieDB
//
//  Created by Suman Sanyal on 08/12/21.
//

import Foundation

struct Language {
    var code: String
    var englishName: String
    var name: String
}
