//
//  swift
//  TheMovieDB
//
//  Created by Suman Sanyal on 08/12/21.
//

import Foundation
import UIKit
import Combine

struct Movie {
    var id: Int64 = -1
    var isAdult = false
    var backdropPath: String?
    var genreIds: [Int64]?
    var originalLanguage: String?
    var originalTitle: String?
    var overview: String?
    var popularity: Double?
    var posterPath: String?
    var releaseDate: String?
    var title: String?
    var isVideo = false
    var averageRating: Double?
    var numberOfRatings: Int?
}

extension Movie {
    static func fromApi(response: ApiMovie) -> Movie {
        let movie = Movie(
            id: response.id,
            isAdult: response.isAdult ?? false,
            backdropPath: response.backdropPath,
            genreIds: response.genreIds,
            originalLanguage: response.originalLanguage,
            originalTitle: response.originalTitle,
            overview: response.overview,
            popularity: response.popularity,
            posterPath: response.posterPath,
            releaseDate: response.releaseDate,
            title: response.title,
            isVideo: response.isVideo ?? false,
            averageRating: response.averageRating,
            numberOfRatings: response.numberOfRatings
        )
        
        return movie
    }
}
