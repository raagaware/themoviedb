//
//  Genre.swift
//  TheMovieDB
//
//  Created by Suman Sanyal on 08/12/21.
//

import Foundation

struct Genre {
    var id: Int64
    var name: String
}
