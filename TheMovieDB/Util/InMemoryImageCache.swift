//
//  InMemoryImageCache.swift
//  TheMovieDB
//
//  Created by Suman Sanyal on 12/12/21.
//

import Foundation
import UIKit

struct InMemoryImageCache: ImageCache {
    
    private var cache = NSCache<NSString, UIImage>()
    
    subscript(url: String) -> UIImage? {
        get {
            let key = NSString(string: url)
            return cache.object(forKey: key)
        }
        
        set(newValue) {
            guard let image = newValue else { return }
            
            let key = NSString(string: url)
            cache.setObject(image, forKey: key)
        }
    }
}
