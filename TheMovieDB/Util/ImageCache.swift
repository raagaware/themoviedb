//
//  ImageCache.swift
//  TheMovieDB
//
//  Created by Suman Sanyal on 07/12/21.
//

import Foundation
import UIKit

protocol ImageCache {
    subscript(url: String) -> UIImage?
        { get set }
}
