//
//  FileImageStore.swift
//  TheMovieDB
//
//  Created by Suman Sanyal on 12/12/21.
//

import Foundation
import UIKit

struct FileImageStore: ImageStore {
    
    let imageDirectoryUrl =
        FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first?
            .appendingPathComponent("tmdb")
            .appendingPathComponent("posters")
    
    let fileManager = FileManager.default
    
    init() {
        try? fileManager.createDirectory(at: imageDirectoryUrl!, withIntermediateDirectories: true, attributes: nil)
    }
    
    subscript(url: String) -> UIImage? {
        get {
            guard let imageUrl = imageDirectoryUrl?.appendingPathComponent(url) else { return nil }
            guard fileManager.fileExists(atPath: imageUrl.path) else { return nil }
            
            return UIImage(contentsOfFile: imageUrl.path)
        }
        
        set(newValue) {
            guard let imageUrl = imageDirectoryUrl?.appendingPathComponent(url) else { return }
            guard let data = newValue?.jpegData(compressionQuality: .greatestFiniteMagnitude) else { return }
            
            do {
                try data.write(to: imageUrl)
            } catch {
                print("\(error)")
            }
        }
    }
    
    func remove(url: String) {
        guard let imageUrl = imageDirectoryUrl?.appendingPathComponent(url) else { return }
        
        do {
            try fileManager.removeItem(at: imageUrl)
        } catch {
            print("\(error)")
        }
    }
}
