//
//  Colors.swift
//  TheMovieDB
//
//  Created by Suman Sanyal on 08/12/21.
//

import Foundation
import SwiftUI

struct Colors {
    static let posterBackground = Color(.sRGB, red: 0, green: 0, blue: 0, opacity: 0.9)
}
