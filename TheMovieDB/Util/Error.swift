//
//  DataError.swift
//  TheMovieDB
//
//  Created by Suman Sanyal on 11/12/21.
//

import Foundation

enum DataError: Error {
    case notFound
    case invalid
}

enum DomainError: Error {
    case invalidArgument
    case configurationNotFound
}
