//
//  ImageStore.swift
//  TheMovieDB
//
//  Created by Suman Sanyal on 12/12/21.
//

import Foundation
import UIKit

protocol ImageStore {
    subscript(url: String) -> UIImage?
        { get set }
    
    func remove(url: String)
}
