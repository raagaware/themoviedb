//
//  TheMovieDBApp.swift
//  TheMovieDB
//
//  Created by Suman Sanyal on 12/12/21.
//

import SwiftUI

@main
struct TheMovieDBApp: App {
    let persistenceController = PersistenceController.shared

    var body: some Scene {
        WindowGroup {
            DependencyFactory.shared.homeView()
                .environment(\.managedObjectContext, persistenceController.container.viewContext)
        }
    }
}
