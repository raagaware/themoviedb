//
//  WebApi.swift
//  TheMovieDB
//
//  Created by Suman Sanyal on 07/12/21.
//

import Combine
import UIKit

protocol TMDBApi {
    
    func fetchImage(from url: String) async throws -> UIImage

    func fetchConfiguration() async throws -> ApiConfiguration

    func fetchCountries() async throws -> [ApiCountry]
    
    func fetchLanguages() async throws -> [ApiLanguage]
    
    func fetchMoviesPlayingNow(page: Int) async throws -> ApiPlayingNow
    
    func fetchGenres() async throws -> ApiGenres
    
}
