//
//  ApiGenres.swift
//  TheMovieDB
//
//  Created by Suman Sanyal on 07/12/21.
//

import Foundation

struct ApiGenres: Codable {
    var value: [ApiGenre]
    
    enum CodingKeys: String, CodingKey {
        case value = "genres"
    }
}
