//
//  ApiCountry.swift
//  TheMovieDB
//
//  Created by Suman Sanyal on 07/12/21.
//

import Foundation

struct ApiCountry: Codable {
    var code: String
    var englishName: String
    
    enum CodingKeys: String, CodingKey {
        case code = "iso_3166_1"
        case englishName = "english_name"
    }
}
