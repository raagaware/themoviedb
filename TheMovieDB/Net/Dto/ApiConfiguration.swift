//
//  ApiConfiguration.swift
//  TheMovieDB
//
//  Created by Suman Sanyal on 07/12/21.
//

import Foundation

struct ApiConfiguration: Codable {
    struct ImageConfiguration: Codable {
        var baseUrl: String
        var secureBaseUrl: String
        var backdropSizes: [String]
        var logoSizes: [String]
        var posterSizes: [String]
        var profileSizes: [String]
        var stillSizes: [String]
        
        enum CodingKeys: String, CodingKey {
            case baseUrl = "base_url"
            case secureBaseUrl = "secure_base_url"
            case backdropSizes = "backdrop_sizes"
            case logoSizes = "logo_sizes"
            case posterSizes = "poster_sizes"
            case profileSizes = "profile_sizes"
            case stillSizes = "still_sizes"
        }
    }
    
    var images: ImageConfiguration
    var changeKeys: [String]
    
    enum CodingKeys: String, CodingKey {
        case images = "images"
        case changeKeys = "change_keys"
    }
}
