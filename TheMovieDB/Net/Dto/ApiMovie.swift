//
//  ApiMovie.swift
//  TheMovieDB
//
//  Created by Suman Sanyal on 07/12/21.
//

import Foundation

struct ApiMovie: Codable, Identifiable {
    var isAdult: Bool?
    var backdropPath: String?
    var genreIds: [Int64]?
    var id: Int64
    var originalLanguage: String?
    var originalTitle: String?
    var overview: String?
    var popularity: Double?
    var posterPath: String?
    var releaseDate: String?
    var title: String?
    var isVideo: Bool?
    var averageRating: Double?
    var numberOfRatings: Int?
    
    init() {
        id = -1
    }
    
    enum CodingKeys: String, CodingKey {
        case isAdult = "adult"
        case backdropPath = "backdrop_path"
        case genreIds = "genre_ids"
        case id = "id"
        case originalLanguage = "original_language"
        case originalTitle = "original_title"
        case overview = "overview"
        case popularity = "popularity"
        case posterPath = "poster_path"
        case releaseDate = "release_date"
        case title = "title"
        case isVideo = "video"
        case averageRating = "vote_average"
        case numberOfRatings = "vote_count"
    }
}
