//
//  ApiGenre.swift
//  TheMovieDB
//
//  Created by Suman Sanyal on 07/12/21.
//

import Foundation

struct ApiGenre: Codable {
    var id: Int64
    var name: String
}
