//
//  ApiPlayingNow.swift
//  TheMovieDB
//
//  Created by Suman Sanyal on 07/12/21.
//

import Foundation

struct ApiPlayingNow: Decodable {
    struct DateRange: Codable {
        var minimum: String
        var maximum: String
    }

    var dateRange: DateRange
    var page: Int
    var totalPages: Int
    var totalResults: Int
    var results: [ApiMovie]
    
    enum CodingKeys: String, CodingKey {
        case dateRange = "dates"
        case page = "page"
        case totalPages = "total_pages"
        case totalResults = "total_results"
        case results = "results"
    }
}
