//
//  ApiLanguage.swift
//  TheMovieDB
//
//  Created by Suman Sanyal on 07/12/21.
//

import Foundation

struct ApiLanguage: Codable {
    var code: String
    var englishName: String
    var name: String
    
    enum CodingKeys: String, CodingKey {
        case code = "iso_639_1"
        case englishName = "english_name"
        case name = "name"
    }
}
