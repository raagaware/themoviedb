//
//  TMDBApiImpl.swift
//  TheMovieDB
//
//  Created by Suman Sanyal on 07/12/21.
//

import UIKit
import Combine

fileprivate let apiKey = "34c902e6393dc8d970be5340928602a7"

struct TMDBApiImpl: TMDBApi {
    
    let session = URLSession(configuration: URLSessionConfiguration.default)

    func fetchConfiguration() async throws -> ApiConfiguration {
        return try await getJson("/configuration")
    }
    
    func fetchCountries() async throws -> [ApiCountry] {
        return try await getJson("/configuration/countries")
    }
    
    func fetchLanguages() async throws -> [ApiLanguage] {
        return try await getJson("/configuration/languages")
    }
    
    func fetchMoviesPlayingNow(page: Int) async throws -> ApiPlayingNow {
        let parameters = [
            "page": "\(page)"
        ]
        return try await getJson("/movie/now_playing", parameters: parameters)
    }
    
    func fetchGenres() async throws -> ApiGenres {
        return try await getJson("/genre/movie/list")
    }
    
    func fetchImage(from urlString: String) async throws -> UIImage {
        let data = try await getData(from: urlString)
        
        guard let image = UIImage(data: data) else {
            throw DataError.invalid
        }
        
        return image
    }

    private func getJson<T>(_ path: String,
                            parameters: [String: String]? = nil) async throws -> T where T: Decodable {
        let baseUrl = "https://api.themoviedb.org/3"
        let urlString = "\(baseUrl)\(path)?api_key=\(apiKey)"
        
        let data = try await getData(from: urlString, parameters: parameters)
        
        let decoder = JSONDecoder()
        let model = try decoder.decode(T.self, from: data)
        return model
    }

    private func getData(from urlString: String,
                         parameters: [String: String]? = nil) async throws -> Data {
        var queryParams = [
            "api_key": apiKey
        ]
        
        if let parameters = parameters {
            queryParams.merge(parameters) { current, _ in current }
        }
        
        var components = URLComponents(string: urlString)!
        components.queryItems = queryParams.map { (key, value) in
            URLQueryItem(name: key, value: value)
        }
        components.percentEncodedQuery = components.percentEncodedQuery?.replacingOccurrences(of: "+", with: "%2B")
        let request = URLRequest(url: components.url!)
        
        let (data, response) = try await session.data(for: request)
        guard (response as? HTTPURLResponse)?.statusCode == 200 else { throw DataError.notFound }
        return data
    }
}
