//
//  MovieRepository.swift
//  TheMovieDB
//
//  Created by Suman Sanyal on 08/12/21.
//

import Foundation
import Combine
import UIKit

protocol MovieRepository {
        
    var nowPlaying: NowPlayingFeed { get }
    var favorites: FavoriteFeed { get }
}
