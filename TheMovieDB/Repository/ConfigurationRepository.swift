//
//  ConfigurationRepository.swift
//  TheMovieDB
//
//  Created by Suman Sanyal on 12/12/21.
//

import Foundation
import Combine

protocol ConfigurationRepository {
    
    var configuration: CurrentValueSubject<Configuration?, Error> { get }
    
    func reload()
}
