//
//  ImageRepositoryImpl.swift
//  TheMovieDB
//
//  Created by Suman Sanyal on 08/12/21.
//

import Foundation
import UIKit
import Combine

class ImageRepositoryImpl: ImageRepository {
    
    private let api: TMDBApi
    private var cache: ImageCache
    private var store: ImageStore
    private var configurationRepository: ConfigurationRepository

    private var imageObserver: AnyCancellable!
    
    private var configuration: ApiConfiguration?
    
    init(api: TMDBApi, cache: ImageCache, store: ImageStore, configurationRepository: ConfigurationRepository) {
        self.api = api
        self.cache = cache
        self.store = store
        self.configurationRepository = configurationRepository
    }
    
    func image(url path: String?) async throws -> UIImage {
        guard let path = path else {
            throw DomainError.invalidArgument
        }
        
        guard let imageBaseUrl = configurationRepository.configuration.value?.imageSecureBaseUrl else {
            throw DomainError.configurationNotFound
        }
        
        let url = "\(imageBaseUrl)/w185/\(path)"

        if let image = cache[url] { return image }
        
        let image = try await api.fetchImage(from: url)
        cache[url] = image
        return image
    }
    
    func storedImage(url path: String?) async throws -> UIImage {
        guard let path = path else { throw DomainError.invalidArgument }

        if let image = self.store[path] { return image }
        
        let image = try await image(url: path)
        store[path] = image
        return image
    }
}
