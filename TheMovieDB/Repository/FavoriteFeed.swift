//
//  FavoriteFeed.swift
//  TheMovieDB
//
//  Created by Suman Sanyal on 09/12/21.
//

import Foundation
import Combine
import CoreData
import UIKit

class FavoriteFeed: MovieFeed {

    @Published var isLoading = false

    let movies = CurrentValueSubject<[Movie], Error>([])

    private let persistenceManager: PersistenceController
    private let imageRepository: ImageRepository

    private lazy var context: NSManagedObjectContext = persistenceManager.viewContext

    private var entities = [MovieEntity]()

    init(persistenceManager: PersistenceController, imageRepository: ImageRepository) {
        self.persistenceManager = persistenceManager
        self.imageRepository = imageRepository
        
        loadInitial()
    }
    
    func loadInitial() {
        entities = persistenceManager.fetchAll(entityName: MovieEntity.name) as [MovieEntity]
        movies.send(entities.map { $0.movie() })
    }
    
    func poster(of movie: Movie) async throws -> UIImage {
        return try await imageRepository.storedImage(url: movie.posterPath)
    }

    func toggleFavorite(_ movie: Movie) {
        if let entity = entity(of: movie) {
            persistenceManager.delete(entity)
            entities = persistenceManager.fetchAll(entityName: MovieEntity.name) as [MovieEntity]
        } else {
            let entity = MovieEntity.newEntity(for: movie, in: context)
            persistenceManager.insert(entity: entity)
            
            entities.append(entity)
        }
        
        let movies = entities.map { $0.movie() }
        self.movies.send(movies)
    }
    
    private func entity(of movie: Movie) -> MovieEntity? {
        let result = entities.filter { $0.id == movie.id }
        return result.isEmpty ? nil : result.first
    }
}
