//
//  MovieFeed.swift
//  TheMovieDB
//
//  Created by Suman Sanyal on 09/12/21.
//

import Foundation
import Combine
import CoreData
import UIKit

protocol MovieFeed {
    
    var isLoading: Bool { get }
    
    var movies: CurrentValueSubject<[Movie], Error> { get }
    
    func loadInitial()
    
    func poster(of movie: Movie) async throws -> UIImage
}

extension MovieFeed {

    func loadMoreIfNecessary(currentMovie: Movie? = nil) {
        // do nothing
    }
    
    func contains(_ movie: Movie) -> Bool {
        self.movies.value.contains { $0.id == movie.id }
    }
}
