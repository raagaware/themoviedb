//
//  MovieRepositoryImpl.swift
//  TheMovieDB
//
//  Created by Suman Sanyal on 08/12/21.
//

import Foundation
import UIKit
import Combine
import CoreData

class MovieRepositoryImpl: MovieRepository {
    
    private let api: TMDBApi
    private let persistenceManager: PersistenceController
    private let imageRepository: ImageRepository
    
    lazy var nowPlaying: NowPlayingFeed = NowPlayingFeed(api: api, imageRepository: imageRepository)
    
    lazy var favorites: FavoriteFeed = FavoriteFeed(persistenceManager: persistenceManager,
                                                    imageRepository: imageRepository)
    
    init(api: TMDBApi, persistenceManager: PersistenceController, imageRepository: ImageRepository) {
        self.api = api
        self.persistenceManager = persistenceManager
        self.imageRepository = imageRepository
    }
}
