//
//  MovieFeed.swift
//  TheMovieDB
//
//  Created by Suman Sanyal on 07/12/21.
//

import Foundation
import Combine
import UIKit
import CoreData

fileprivate let preferredPrefetchThreshold = 4

fileprivate let placeholderImage = UIImage(systemName: "tv.fill")!

class NowPlayingFeed: MovieFeed {
        
    @Published var isLoading = false
    
    let movies = CurrentValueSubject<[Movie], Error>([])
    
    private let api: TMDBApi
    private let imageRepository: ImageRepository
    
    private var configutation: ApiConfiguration?
    
    private var nextPage = 1
    private var lastPage: Int?

    init(api: TMDBApi, imageRepository: ImageRepository) {
        self.api = api
        self.imageRepository = imageRepository
    }

    func loadInitial() {
        self.loadMoreIfNecessary(currentMovie: nil)
    }
    
    func poster(of movie: Movie) async throws -> UIImage {
        return try await imageRepository.image(url: movie.posterPath)
    }

    func loadMoreIfNecessary(currentMovie: Movie? = nil) {
        guard shouldLoadMore(currentMovie: currentMovie) else { return }
        
        self.isLoading = true
        
        Task {
            do {
                let nowPlaying = try await api.fetchMoviesPlayingNow(page: nextPage)
                self.isLoading = false
                
                self.nextPage = nowPlaying.page + 1
                self.lastPage = nowPlaying.totalPages
                
                let movies = nowPlaying.results.map { Movie.fromApi(response: $0) }
                self.movies.send(movies)
            } catch (let error) {
                self.isLoading = false
                self.movies.send(completion: .failure(error))
            }
        }
    }
    
    private func shouldLoadMore(currentMovie: Movie? = nil) -> Bool {
        guard !self.isLoading else { return false }
        guard let lastPage = lastPage else { return true }
        guard let currentMovie = currentMovie else { return true }
        guard !movies.value.isEmpty else { return true }
        
        let prefetchThreshold = Swift.min(preferredPrefetchThreshold, movies.value.count)
        let lastFetchedMovies = movies.value.suffix(prefetchThreshold)
        
        return (nextPage < lastPage) && lastFetchedMovies.contains { $0.id == currentMovie.id }
    }
}
