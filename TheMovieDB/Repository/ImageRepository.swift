//
//  ImageRepository.swift
//  TheMovieDB
//
//  Created by Suman Sanyal on 08/12/21.
//

import Foundation
import UIKit
import Combine

protocol ImageRepository {
    func image(url path: String?) async throws -> UIImage
    func storedImage(url path: String?) async throws -> UIImage
}
