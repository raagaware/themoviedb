//
//  ConfigurationRepositoryImpl.swift
//  TheMovieDB
//
//  Created by Suman Sanyal on 12/12/21.
//

import Foundation
import Combine

class ConfigurationRepositoryImpl: ConfigurationRepository {
    
    var configuration = CurrentValueSubject<Configuration?, Error>(nil)

    private let api: TMDBApi
        
    init(with api: TMDBApi) {
        self.api = api
    }
    
    func reload() {
        Task {
            do {
                let result = try await api.fetchConfiguration()
                let configuration = Configuration.fromApi(response: result)
                self.configuration.send(configuration)
            } catch (let error) {
                self.configuration.send(completion: .failure(error))
            }
        }
    }
}
