//
//  FavoritesView.swift
//  TheMovieDB
//
//  Created by Suman Sanyal on 08/12/21.
//

import SwiftUI

struct FavoritesView: View {
    
    var viewModel: FavoritesViewModel
    
    var body: some View {
        NavigationView {
            ShowcaseView(viewModel: viewModel, showFavorite: false) {
                Text("Favorites.Empty.Text")
                    .font(.title)
            }
            .padding(.top)
            
            .navigationTitle(Text("Favorites.Title"))
        }
        .navigationViewStyle(.stack)
    }
}

struct FavoritesView_Previews: PreviewProvider {
    static var previews: some View {
        let api = TMDBApiImpl()
        let imageCache = InMemoryImageCache()
        let imageStore = FileImageStore()
        let configurationRepository = ConfigurationRepositoryImpl(with: api)
        let imageRepository = ImageRepositoryImpl(api: api,
                                                  cache: imageCache,
                                                  store: imageStore,
                                                  configurationRepository: configurationRepository)
        let persistenceManager = PersistenceController(inMemory: true)
        let movieRepository = MovieRepositoryImpl(api: api,
                                                  persistenceManager: persistenceManager,
                                                  imageRepository: imageRepository)
        let viewModel = FavoritesViewModel(with: movieRepository)
        
        FavoritesView(viewModel: viewModel)
    }
}
