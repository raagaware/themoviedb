//
//  NowPlayingView.swift
//  TheMovieDB
//
//  Created by Suman Sanyal on 07/12/21.
//

import SwiftUI

struct NowPlayingView: View {
    
    var viewModel: NowPlayingViewModel
    
    var body: some View {
        NavigationView {
            ShowcaseView(viewModel: viewModel, showFavorite: true) {
                Text("NowPlaying.Empty.Text")
                    .font(.title)
            }
            .padding(.top)
            
            .navigationTitle(Text("NowPlaying.Title"))
        }
        .navigationViewStyle(.stack)
    }
}

struct PlayingNowView_Previews: PreviewProvider {
    static var previews: some View {
        let api = TMDBApiImpl()
        let imageCache = InMemoryImageCache()
        let imageStore = FileImageStore()
        let configurationRepository = ConfigurationRepositoryImpl(with: api)
        let imageRepository = ImageRepositoryImpl(api: api,
                                                  cache: imageCache,
                                                  store: imageStore,
                                                  configurationRepository: configurationRepository)
        let persistenceManager = PersistenceController(inMemory: true)
        let repository = MovieRepositoryImpl(api: api,
                                             persistenceManager: persistenceManager,
                                             imageRepository: imageRepository)
        let viewModel = NowPlayingViewModel(with: repository)
        
        NowPlayingView(viewModel: viewModel)
    }
}
