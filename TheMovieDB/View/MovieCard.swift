//
//  MovieCard.swift
//  TheMovieDB
//
//  Created by Suman Sanyal on 07/12/21.
//

import SwiftUI
import Combine

struct MovieCard<DetailsView: View>: View {
    
    @Environment(\.horizontalSizeClass) var sizeClass
    @Environment(\.colorScheme) var colorScheme
    
    var poster: UIImage
    var title: String
    var showFavorite: Bool
    var isFavorite: Bool
    
    var detailsView: DetailsView

    var toggleFavorite: () -> Void

    var body: some View {
        ZStack {
            if colorScheme == .dark {
                Rectangle()
                    .fill(Colors.posterBackground)
                    .border(.white, width: 2)
            } else {
                Rectangle()
                    .fill(Colors.posterBackground)
            }
            
            if sizeClass == .compact {
                CompactLayout(poster: poster,
                              title: title,
                              showFavorite: showFavorite,
                              isFavorite: isFavorite,
                              detailsView: detailsView,
                              toggleFavorite: toggleFavorite)
            } else {
                RegularLayout(poster: poster,
                              title: title,
                              showFavorite: showFavorite,
                              isFavorite: isFavorite,
                              detailsView: detailsView,
                              toggleFavorite: toggleFavorite)
            }
        }
    }
}

struct CompactLayout<DetailsView: View>: View {
    
    @Environment(\.colorScheme) var colorScheme

    var poster: UIImage
    var title: String
    var showFavorite: Bool
    var isFavorite: Bool
    
    var detailsView: DetailsView

    var toggleFavorite: () -> Void

    let imageSize = CGFloat(185)

    var body: some View {
        let topPadding: CGFloat = colorScheme == .dark ? 4 : 0
        
        VStack(alignment: .center) {
            Image(uiImage: poster)
                .resizable()
                .scaledToFit()
                .frame(width: imageSize, height: imageSize)
                .padding(.top, topPadding)

            Text(title)
                .font(.headline)
                .bold()
                .foregroundColor(.white)
                .padding(.top)
            
            HStack {
                if showFavorite {
                    Button {
                        toggleFavorite()
                    } label: {
                        Image(systemName: isFavorite ? "heart.fill" : "heart")
                            .foregroundColor(.white)
                            .font(.title2)
                    }
                }

                Spacer()
                
                NavigationLink(destination: detailsView) {
                    Image(systemName: "info.circle")
                        .foregroundColor(.white)
                        .font(.title2)
                }
            }.padding()
        }
    }
}

struct RegularLayout<DetailsView: View>: View {
    
    @Environment(\.colorScheme) var colorScheme

    var poster: UIImage
    var title: String
    var showFavorite: Bool
    var isFavorite: Bool
    
    var detailsView: DetailsView

    var toggleFavorite: () -> Void

    let imageSize = CGFloat(185)

    var body: some View {
        let topPadding: CGFloat = colorScheme == .dark ? 4 : 0
        
        VStack {
            HStack(alignment: .bottom) {
                Spacer()

                if showFavorite {
                    Button {
                        toggleFavorite()
                    } label: {
                        Image(systemName: isFavorite ? "heart.fill" : "heart")
                            .foregroundColor(.white)
                            .font(.title2)
                    }
                }

                Spacer()

                Image(uiImage: poster)
                    .resizable()
                    .scaledToFit()
                    .frame(width: imageSize, height: imageSize)
                    .padding(.top, topPadding)

                Spacer()
                
                NavigationLink(destination: detailsView) {
                    Image(systemName: "info.circle")
                        .foregroundColor(.white)
                        .font(.title2)
                }

                Spacer()
            }
            
            Text(title)
                .font(.headline)
                .bold()
                .foregroundColor(.white)
                .padding(.top)
        }
        .padding(.leading)
        .padding(.trailing)
        .padding(.bottom)
    }
}

struct MovieCard_Previews: PreviewProvider {
    static let placeholderImage = UIImage(named: "MoviePlaceholder")!

    static var previews: some View {
        MovieCard(poster: placeholderImage,
                  title: "Some movie",
                  showFavorite: true,
                  isFavorite: false,
                  detailsView: DependencyFactory.shared.movieView(movie: Movie())) {
            // Do nothing
        }
    }
}
