//
//  MovieView.swift
//  TheMovieDB
//
//  Created by Suman Sanyal on 07/12/21.
//

import Combine
import SwiftUI

struct MovieView: View {
    @EnvironmentObject var viewModel: MovieViewModel
    
    var body: some View {
        ScrollView {
            ZStack {
                Rectangle()
                    .fill(Colors.posterBackground)
                
                Image(uiImage: viewModel.poster)
                    .resizable()
                    .scaledToFit()
                    .frame(width: viewModel.imageSize, height: viewModel.imageSize)
                    .padding(.top, 0)
            }

            Group {
                Text(viewModel.title)
                    .font(.title)
                
                Text(viewModel.overview)
            }
            .padding()

            .navigationTitle(viewModel.releaseDate)
        }
        .padding(.top)
        .onAppear {
            viewModel.refresh()
        }
    }
}

struct MovieView_Previews: PreviewProvider {
    static var previews: some View {
        let movie = Movie()
        DependencyFactory.shared.movieView(movie: movie)
    }
}
