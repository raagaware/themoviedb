//
//  ShowcaseView.swift
//  TheMovieDB
//
//  Created by Suman Sanyal on 09/12/21.
//

import SwiftUI

struct ShowcaseView<ViewModel, EmptyView>: View where ViewModel: ShowcaseViewModel, EmptyView: View {
    
    @ObservedObject var viewModel: ViewModel
    
    var showFavorite: Bool
    
    var emptyView: () -> EmptyView
    
    let layout = [
        GridItem(.adaptive(minimum: 300))
    ]
    
    var body: some View {
        if viewModel.movies.isEmpty {
            emptyView()
        } else {
            ScrollView {
                LazyVGrid(columns: layout, spacing: 20) {
                    ForEach(viewModel.movies, id: \.id) { movie in
                        MovieCard(poster: viewModel.poster(of: movie),
                                  title: viewModel.title(of: movie),
                                  showFavorite: showFavorite,
                                  isFavorite: viewModel.isFavorite(movie),
                                  detailsView: DependencyFactory.shared.movieView(movie: movie)) {
                            viewModel.toggleFavorite(movie)
                        }
                        .onAppear {
                            viewModel.loadMoreIfNecessary(currentMovie: movie)
                        }
                    }
                }
                .padding(.horizontal)
            }
        }
    }
}

struct ShowcaseView_Previews: PreviewProvider {
    
    static var previews: some View {
        let api = TMDBApiImpl()
        let imageCache = InMemoryImageCache()
        let imageStore = FileImageStore()
        let configurationRepository = ConfigurationRepositoryImpl(with: api)
        let imageRepository = ImageRepositoryImpl(api: api,
                                                  cache: imageCache,
                                                  store: imageStore,
                                                  configurationRepository: configurationRepository)
        let repository = MovieRepositoryImpl(api: api,
                                             persistenceManager: PersistenceController.shared,
                                             imageRepository: imageRepository)
        let viewModel = FavoritesViewModel(with: repository)
        
        ShowcaseView(viewModel: viewModel, showFavorite: true) {
            Text("Nothing yet")
        }
    }
}
