//
//  HomeView.swift
//  TheMovieDB
//
//  Created by Suman Sanyal on 08/12/21.
//

import SwiftUI

struct HomeView: View {
    
    @ObservedObject var viewModel: HomeViewModel
    
    var body: some View {
        if viewModel.isLoading {
            ProgressView("Configurations.Load.Text")
        } else {
            TabView {
                DependencyFactory.shared.nowPlayingView()
                    .tabItem {
                        Image(systemName: "tv.fill")
                        Text("NowPlaying.Title")
                    }
                
                DependencyFactory.shared.favoritesView()
                    .tabItem {
                        Image(systemName: "heart.fill")
                        Text("Favorites.Title")
                    }
            }
        }
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        DependencyFactory.shared.homeView()
    }
}
