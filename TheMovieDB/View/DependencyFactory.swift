//
//  ViewFactory.swift
//  TheMovieDB
//
//  Created by Suman Sanyal on 08/12/21.
//

import Foundation
import SwiftUI

struct DependencyFactory {
    
    static let shared = DependencyFactory()
    
    let configurationRepository: ConfigurationRepository
    let movieRepository: MovieRepository
    let imageRepository: ImageRepository

    private init() {
        let api: TMDBApi = TMDBApiImpl()
        let persistenceManager = PersistenceController.shared
        
        configurationRepository = ConfigurationRepositoryImpl(with: api)
        
        let imageCache = InMemoryImageCache()
        let imageStore = FileImageStore()
        imageRepository = ImageRepositoryImpl(api: api,
                                              cache: imageCache,
                                              store: imageStore,
                                              configurationRepository: configurationRepository)

        movieRepository = MovieRepositoryImpl(api: api,
                                              persistenceManager: persistenceManager,
                                              imageRepository: imageRepository)
    }
    
    @ViewBuilder
    func homeView() -> some View {
        let viewModel = HomeViewModel(with: configurationRepository)
        HomeView(viewModel: viewModel)
    }
    
    @ViewBuilder
    func nowPlayingView() -> some View {
        let viewModel = NowPlayingViewModel(with: movieRepository)
        NowPlayingView(viewModel: viewModel)
    }
    
    @ViewBuilder
    func favoritesView() -> some View {
        let viewModel = FavoritesViewModel(with: movieRepository)
        FavoritesView(viewModel: viewModel)
    }
    
    @ViewBuilder
    func movieView(movie: Movie) -> some View {
        let viewModel = MovieViewModel(movie: movie, imageRepository: imageRepository)
        MovieView()
            .environmentObject(viewModel)
    }
}
