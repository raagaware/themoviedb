//
//  MovieEntityExtension.swift
//  TheMovieDB
//
//  Created by Suman Sanyal on 10/12/21.
//

import Foundation
import CoreData

extension MovieEntity {
    
    static let name = "MovieEntity"
    
    func movie() -> Movie {
        let movie = Movie(
            id: self.id,
            isAdult: self.isAdult,
            backdropPath: self.backdrop,
    //        genreIds: response.genreIds,
    //        originalLanguage: response.originalLanguage,
            originalTitle: self.originalTitle,
            overview: self.overview,
            popularity: self.popularity,
            posterPath: self.posterPath,
            releaseDate: self.releaseDate,
            title: self.title,
            isVideo: self.isVideo,
            averageRating: self.averageRating,
            numberOfRatings: Int(self.numberOfRatings)
        )
        
        return movie
    }
    
    static func newEntity(for movie: Movie, in context: NSManagedObjectContext) -> MovieEntity {
        let entity = MovieEntity(context: context)
        
        entity.id = movie.id
        entity.isAdult = movie.isAdult
        entity.backdrop = movie.backdropPath
//        movie.genreIds = response.genreIds
//        movie.originalLanguage = response.originalLanguage
        entity.originalTitle = movie.originalTitle
        entity.overview = movie.overview
        entity.popularity = movie.popularity ?? 0
        entity.posterPath = movie.posterPath
        entity.releaseDate = movie.releaseDate
        entity.title = movie.title
        entity.isVideo = movie.isVideo
        entity.averageRating = movie.averageRating ?? 0
        entity.numberOfRatings = Int32(movie.numberOfRatings ?? 0)
        
        return entity
    }
}
