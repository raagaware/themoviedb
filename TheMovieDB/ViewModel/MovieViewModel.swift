//
//  MovieViewModel.swift
//  TheMovieDB
//
//  Created by Suman Sanyal on 09/12/21.
//

import Foundation
import UIKit
import Combine

fileprivate let placeholderImage = UIImage(named: "MoviePlaceholder")!

class MovieViewModel: ObservableObject {
    
    var movie: Movie
    var imageRepository: ImageRepository
    
    @Published var title: String
    @Published var overview: String
    @Published var poster: UIImage
    @Published var releaseDate: String
    
    let imageSize = CGFloat(400)
        
    init(movie: Movie, imageRepository: ImageRepository) {
        self.movie = movie
        self.imageRepository = imageRepository
        
        self.title = movie.title ?? "No title"
        self.overview = movie.overview ?? "No overview found"
        self.poster = placeholderImage
        self.releaseDate = MovieViewModel.formattedReleaseDate(dateString: movie.releaseDate)
    }
    
    func refresh() {
        guard let posterPath = movie.posterPath else { return }
        
        Task {
            do {
                let poster = try await imageRepository.image(url: posterPath)
                DispatchQueue.main.async {
                    self.poster = poster
                }
            } catch (let error) {
                print("\(error)")
            }
        }
    }
    
    private static func formattedReleaseDate(dateString: String?) -> String {
        guard let dateString = dateString else { return "" }

        let inputDateFormatter = DateFormatter()
        inputDateFormatter.dateFormat = "yyyy-MM-dd"
        
        let outputDateFormatter = DateFormatter()
        outputDateFormatter.dateFormat = "MMM dd, yyyy"
        
        guard let date = inputDateFormatter.date(from: dateString) else { return "" }
        
        return outputDateFormatter.string(from: date)
    }
}
