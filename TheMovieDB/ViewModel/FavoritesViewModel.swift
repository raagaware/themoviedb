//
//  FavoritesViewModel.swift
//  TheMovieDB
//
//  Created by Suman Sanyal on 11/12/21.
//

import Foundation
import Combine
import UIKit

fileprivate let placeholderImage = UIImage(named: "MoviePlaceholder")!

class FavoritesViewModel: ShowcaseViewModel {
    
    @Published var isLoading = false
    @Published var movies = [Movie]()
    @Published var posters = [Int64: UIImage]()

    private let movieRepository: MovieRepository
    private let feed: FavoriteFeed

    private var movieObserver: AnyCancellable!

    required init(with repository: MovieRepository) {
        movieRepository = repository
        feed = repository.favorites
        
        movieObserver = feed.movies
            .receive(on: DispatchQueue.main)
            .sink { completion in
            switch completion {
            case .finished:
                break
                
            case .failure(let error):
                print("\(error)")
            }
        } receiveValue: { [weak self] movies in
            self?.movies = movies
        }

        feed.loadInitial()
    }
    
    func title(of movie: Movie) -> String {
        movie.title ?? "No title found"
    }
    
    func poster(of movie: Movie) -> UIImage {
        if let poster = self.posters[movie.id] {
            return poster
        }
        
        Task {
            do {
                let poster = try await feed.poster(of: movie)
                DispatchQueue.main.async {
                    self.posters[movie.id] = poster
                }
            } catch (let error) {
                print("\(error)")
            }
        }
        
        return placeholderImage
    }
    
    func isFavorite(_ movie: Movie) -> Bool {
        true
    }
    
    func toggleFavorite(_ movie: Movie) {
        feed.toggleFavorite(movie)
    }
}
