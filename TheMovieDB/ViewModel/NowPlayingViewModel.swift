//
//  NowPlayingViewModel.swift
//  TheMovieDB
//
//  Created by Suman Sanyal on 08/12/21.
//

import Foundation
import UIKit
import Combine

fileprivate let placeholderImage = UIImage(named: "MoviePlaceholder")!

class NowPlayingViewModel: ShowcaseViewModel {
    
    @Published var isLoading = false
    @Published var movies = [Movie]()
    @Published var posters = [Int64: UIImage]()
    @Published var favorites = [Int64]()
    
    private let movieRepository: MovieRepository
    private let nowPlayingFeed: NowPlayingFeed
    private let favoriteFeed: FavoriteFeed
    
    private var favoriteObserver: AnyCancellable!
    private var nowPlayingObserver: AnyCancellable!
    
    required init(with repository: MovieRepository) {
        movieRepository = repository
        nowPlayingFeed = repository.nowPlaying
        favoriteFeed = repository.favorites
        
        loadInitial()
    }
    
    func loadInitial() {
        favoriteObserver = favoriteFeed.movies
            .sink(receiveCompletion: { completion in
                switch completion {
                case .finished:
                    break
                    
                case .failure(let error):
                    print("\(error)")
                }
            }, receiveValue: { [weak self] movies in
                self?.favorites = movies.map { $0.id }
            })
        
        nowPlayingObserver = nowPlayingFeed.movies
            .receive(on: DispatchQueue.main)
            .sink { completion in
            switch completion {
            case .finished:
                break
                
            case .failure(let error):
                print("\(error)")
            }
        } receiveValue: { [weak self] nextBatch in
            self?.movies.append(contentsOf: nextBatch)
        }

        nowPlayingFeed.loadInitial()
    }
    
    func loadMoreIfNecessary(currentMovie: Movie) {
        nowPlayingFeed.loadMoreIfNecessary(currentMovie: currentMovie)
    }
    
    func poster(of movie: Movie) -> UIImage {
        if let poster = self.posters[movie.id] {
            return poster
        }
        
        Task {
            do {
                let poster = try await nowPlayingFeed.poster(of: movie)
                DispatchQueue.main.async {
                    self.posters[movie.id] = poster
                }
            } catch (let error) {
                print("\(error)")
            }
        }

        return placeholderImage
    }
    
    func isFavorite(_ movie: Movie) -> Bool {
        favoriteFeed.contains(movie)
    }
    
    func toggleFavorite(_ movie: Movie) {
        favoriteFeed.toggleFavorite(movie)
    }
}
