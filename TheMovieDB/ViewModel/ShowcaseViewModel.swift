//
//  ShowcaseViewModel.swift
//  TheMovieDB
//
//  Created by Suman Sanyal on 11/12/21.
//

import Foundation
import Combine
import UIKit

protocol ShowcaseViewModel: ObservableObject {
    
    var isLoading: Bool { get }
    
    var movies: [Movie] { get }
    
    init(with repository: MovieRepository)
        
    func loadMoreIfNecessary(currentMovie: Movie)
    
    func title(of movie: Movie) -> String
    func poster(of movie: Movie) -> UIImage
    func isFavorite(_ movie: Movie) -> Bool
    func toggleFavorite(_ movie: Movie)
}

extension ShowcaseViewModel {
    
    func loadMoreIfNecessary(currentMovie: Movie) {
        // do nothing by default
    }

    func title(of movie: Movie) -> String {
        movie.title ?? "No title found"
    }
}
