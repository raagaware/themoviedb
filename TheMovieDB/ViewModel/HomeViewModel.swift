//
//  HomeViewModel.swift
//  TheMovieDB
//
//  Created by Suman Sanyal on 12/12/21.
//

import Foundation
import Combine

class HomeViewModel: ObservableObject {
    
    @Published var isLoading: Bool
    
    private var observer: AnyCancellable!
    
    init(with configurationRepository: ConfigurationRepository) {
        isLoading = true
        observer = configurationRepository.configuration
            .sink { [weak self] completion in
                switch completion {
                case .finished:
                    self?.isLoading = false
                    break
                    
                case .failure(let error):
                    self?.isLoading = false
                    print("\(error)")
                }
            } receiveValue: { [weak self] _ in
                self?.isLoading = false
            }
        
        configurationRepository.reload()
    }
}
